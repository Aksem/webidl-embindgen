WEBIDL_TYPE_TO_JS_TYPE = dict(
  Void='',
  UnsignedLong='number',
  Double='number'
)

def webidl_type_to_js_type(webidl_type):
  try:
    return WEBIDL_TYPE_TO_JS_TYPE[webidl_type]
  except KeyError:
    return webidl_type


def generate_interface_docstring(inteface):
  return f'''
/**
 * @class
 */
'''

def generate_interface_method_docstring(inteface, method):
  print(method)
  print(method.signatures())
  """ 
   * @param {{string}} color - The shoe's color.
 * @return {{string}} The blended color.
  """
  signature = None
  if len(method.signatures()) > 0:
    signature = method.signatures()[0]
  
  if signature is None:
    return ''

  parameter_strings = []
  for idl_argument in signature[1]:
    # TODO: type to js type
    parameter_strings.append(f'@param {{{webidl_type_to_js_type(idl_argument.type.name)}}} {idl_argument.identifier.name}')
  parameters = '\n * ' + '\n * '.join(parameter_strings)

  return_type = webidl_type_to_js_type(signature[0].name)
  return_string = ''
  if return_type != '':
    return_string = f'\n * @returns {{{return_type}}}'

  return f'''
/**
 * @method{parameters}{return_string}
 */'''
