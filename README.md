# Webidl-embingen fork

Original source: https://github.com/emscripten-core/emscripten/blob/main/tools/webidl_binder.py

Main difference with original version:

* proper support of namespaced enums
* callback support
* generation of JSDoc docs based on webidl

After all needed for my project changes will be finished, I'll try to merge them in original repository.
