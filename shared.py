import os
import shutil
import stat


# Attempts to delete given possibly nonexisting or read-only directory tree or filename.
# If any failures occur, the function silently returns without throwing an error.
def try_delete(pathname):
  try:
    os.unlink(pathname)
  except OSError:
    pass
  if not os.path.exists(pathname):
    return
  try:
    shutil.rmtree(pathname, ignore_errors=True)
  except IOError:
    pass
  if not os.path.exists(pathname):
    return

  write_bits = stat.S_IWRITE | stat.S_IWGRP | stat.S_IWOTH

  def is_writable(path):
    return (os.stat(path).st_mode & write_bits) == write_bits

  def make_writable(path):
    os.chmod(path, os.stat(path).st_mode | write_bits)

  # Some tests make files and subdirectories read-only, so rmtree/unlink will not delete
  # them. Force-make everything writable in the subdirectory to make it
  # removable and re-attempt.
  if not is_writable(pathname):
    make_writable(pathname)

  if os.path.isdir(pathname):
    for directory, subdirs, files in os.walk(pathname):
      for item in files + subdirs:
        i = os.path.join(directory, item)
        make_writable(i)

  try:
    shutil.rmtree(pathname, ignore_errors=True)
  except IOError:
    pass
